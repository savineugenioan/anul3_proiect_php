<!DOCTYPE html>
<html lang="en">
<?php
include("head.html");
$def_source = "dragon.jpg";
?>
<link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<body>
    <?php
    include('navbar_admin.php');
    ?>
    </br>
    <form class="form-group" enctype="multipart/form-data" method="post" id="form" action="util/adaug_produs.php" onsubmit="return validate()">
        <div class="form-group" style="vertical-align:middle;">
            <img width="200px" style="margin: auto auto;display:block;" height="200px" id="image" src='<?php echo $def_source; ?>' onerror='this.src ="<?php echo $def_source; ?>"' alt='...'></img>
            <label>Poza</label>
            <input type="file" class="form-control mb-4" name="image" id="img_image" accept="image/png, image/jpeg" onchange="handleFiles(this.files)">
            <label>Denumire Produs</label>
            <input width="100%" class="form-control mb-4"  type="text" id="denumire" class="fadeIn second" name="denumire" placeholder="Denumire" >
            <label>Pret Unitar</label>
            <input width="100%" class="form-control mb-4" type="number" id="pret" class="fadeIn third" min="0" name="pret" placeholder="Pret" >
        </div>
        <button type="submit" id="submit" class="btn btn-info btn-block my-4">Salveaza Datele</button>
    </form>

</body>
<script>
    var checked_photo = 0;
    document.getElementById('form').action += '?checked_photo=0';

    function handleFiles(files) {

        if (FileReader && files && files.length) {
            var fr = new FileReader();
            fr.onload = function() {
                document.getElementById('image').src = fr.result;
                checked_photo = 1;
                var x = document.getElementById('form').action;
                x = x.substr(0, x.length - 1);
                document.getElementById('form').action = x + checked_photo;
                console.log(x);
            }
            fr.readAsDataURL(files[0]);
        }
    }

    function change_photo() {
        return checked_photo;
    }
</script>
<script>
    function validate() {
        let denumire = document.getElementById("denumire").value;
        let pret = document.getElementById("pret").value;
        let alert = document.getElementById("alert");
        if (denumire == "" || pret == "") {
            if (alert != null)
                return false;
            string = '<div class="alert alert-danger" id="alert" role="alert"> Completati toate campurile!</div>';
            var div = document.createElement('div');
            div.innerHTML = string;
            var list = document.getElementById("form");
            list.insertBefore(div, list.childNodes[3]);
            return false;
        }
        return true;
    }
</script>

</html>