<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  echo file_get_contents("head.html");
  include("navbar_admin.php");
  $def_source = "dragon.jpg";
  ?>
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/comenzi.css">
</head>


<body>
  <?php
  $id_user = $_SESSION['id_User'];
  ?>
  <button class="btn btn-primary" style ="margin:-30px auto 40px auto;display:block;" onclick="redirect()">Adaugare Produs</button>
  <table style="position:relative;width:100%" id="dtBasicExample" class="table table-dark table-striped table-bordered " cellspacing="0" width="100%">
    <thead>
      <tr>
        <th class="th-sm" scope="col">Poza</th>
        <th class="th-sm" scope="col">Nume Produs</th>
        <th class="th-sm" scope="col">Pret Unitar</th>
        <th class="th-sm" scope="col">Edit</th>
        <th class="th-sm" scope="col">Delete</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $query = mysqli_query($dbconnect, "SELECT * FROM Produse") or die(mysqli_error($dbconnect));

      while ($row = mysqli_fetch_array($query)) {
        echo "
    <tr>
      <td><img class='t_img' src='$row[3]' onerror='this.src =" . '"' . $def_source . '"' . "'</td>
      <th scope='row'>$row[1]</th>
      <td>$row[2]</td>
      <td><a href='editare_produs.php?id=$row[0]'>Edit</a></td>
      <td><a href='util/delete_produs.php?id=$row[0]&img=$row[3]' onclick='return alerta()'>Delete</a></td>
    </tr>";
      }
      ?>
    </tbody>
  </table>
  <script>
    $(document).ready(function() {
      $('#dtBasicExample').DataTable({
        "pagingType": "simple"
      });
      $('.dataTables_length').addClass('bs-select');
    });

    function alerta() {
      var r = confirm("Doriti sa stergeti acest produs?");
      if (r == true) {
        return true
      } else {
        return false;
      }
    }
    function redirect(){
      window.location.assign('adaugare_produs.php');
    }
  </script>
</body>

</html>