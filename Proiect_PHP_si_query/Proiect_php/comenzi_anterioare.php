<!DOCTYPE html>
<html lang="en">
<!DOCTYPE html>
<html lang="en">
<head>
  <?php
  echo file_get_contents("head.html");
  include("navbar.php");
  ?>
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/comenzi.css">
</head>
<?php

$id_user=$_SESSION['id_User'];
$sql ="SELECT * FROM COMENZI_DETALII cd INNER JOIN COMENZI c ON c.Id_comanda = cd.Id_Comanda  WHERE c.Id_User = $id_user";

?>

<?php
  $id_user=$_SESSION['id_User'];
  ?>
<table  style="position:relative;width:100%"id="dtBasicExample" class="table table-dark table-striped table-bordered " cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm"scope="col">Nr. Comanda</th>
      <th class="th-sm"scope="col">Produs</th>
      <th class="th-sm"scope="col">Cantitate</th>
      <th class="th-sm"scope="col">Pret Unitar</th>
      <th class="th-sm"scope="col">Pret Total</th>
    </tr>
  </thead>
  <tbody>
  <?php
    $query = mysqli_query($dbconnect, $sql) or die(mysqli_error($dbconnect));

    while($row = mysqli_fetch_array($query)){
    echo "
    <tr>
      <th scope='row'>$row[4]</th>
      <td>$row[1]</td>
      <td>$row[2]</td>
      <td>$row[3]</td>
      <td>".$row[2]*$row[3]."</td>
    </tr>";
    }
    ?>
  </tbody>
</table>
<script>
  $(document).ready(function () {
  $('#dtBasicExample').DataTable({
    "pagingType": "simple"
  });
  $('.dataTables_length').addClass('bs-select');
});
</script>