    <?php
    include('util/DB.php');
    if (isset($_SESSION['isLogged']))
      $isLogged = $_SESSION['isLogged'];
    else
      $isLogged = 0;
    $suma = 0;
    $is_admin= 0;
    if(isset($_SESSION['id_User'])){
      $id_User=$_SESSION['id_User'];
      $query = mysqli_query($dbconnect, "SELECT is_admin FROM USERS WHERE id_User=$id_User;")
      or die(mysqli_error($dbconnect));
      $is_admin = mysqli_fetch_all($query)[0][0];
      echo "<script>var is_admin=$is_admin;</script>";
    }

    $query = mysqli_query($dbconnect, "SELECT * FROM Produse")
      or die(mysqli_error($dbconnect));


    while ($row = mysqli_fetch_array($query)) {
      if (isset($_SESSION['cart'][$row[0]]['count']))
        $suma +=$_SESSION['cart'][$row[0]]['count'];
    }
    ?>

    <script>
      function create_dreapta() {
        var elem;
        if (<?php echo $isLogged; ?> == 1) {
          elem =
            '<li class="nav-item dropdown" id="dropdown">' + 
            '<a class=" dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">' +
            '<img src="util/iki.jpg" alt="Avatar" id="poza_profil" style="width:45px">' +
            '</a>' +
            '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">' +
            '<a class="dropdown-item" href="comenzi_anterioare.php">Comenzi Anterioare</a>'+
            '<a class="dropdown-item" href="date_personale.php">Date Personale</a>';
            if(is_admin ==1){
              elem+=
              '<a class="dropdown-item" href="produse_admin.php">Meniu Admin</a>';
            }
            elem+=
            '<div class="dropdown-divider"></div>' +
            '<a class="dropdown-item" href="home.php" onclick="deconectare()">Deconectare</a>' +
            '</div>' +
            '</li>';

        } else {
          elem =
            '<li class="nav-item" id="btn_conectare">' +
            '<button class="btn btn-primary" ><a style="color:white" href="login.php">Login</a></button>' +
            '</li>';
        }
        document.getElementById('navbar_dreapta').innerHTML += elem;
      }

      function deconectare() {
        sessionStorage.setItem('isLogged', 0);
        $.post('util/login_verif.php', {
          deconect: 1
        });
      }

      function verif_login() {
        $.get("util/isLogged.php",{},function(data){
          console.log(data);
          if(data ==1)
            window.location = 'shopping_cart.php';
          else
            window.location = 'login.php';

        })  
      }
    </script>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="home.php">
              <svg class="bi bi-house-fill" width="1.5em" height="1.5em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.5 12.995V16.5a.5.5 0 01-.5.5H4a.5.5 0 01-.5-.5v-7a.5.5 0 01.146-.354l6-6a.5.5 0 01.708 0l6 6a.5.5 0 01.146.354v7a.5.5 0 01-.5.5h-4a.5.5 0 01-.5-.5V13c0-.25-.25-.5-.5-.5H9c-.25 0-.5.25-.5.495z">
                </path>
                <path fill-rule="evenodd" d="M15 4.5V8l-2-2V4.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd">
                </path>
              </svg>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="produse.php">Produse</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="locatii.php">Locatii</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="despre_noi.php">Despre noi</a>
          </li>

        </ul>
        <ul class="navbar-nav" id="navbar_dreapta">
          <li class="nav-item dropdown" >
            <a class="nav-brand" href="#" onclick="verif_login()">
              <img src="util/cart.png" alt="*Cart" height="40px" width="40px">
            </a>
          <li  style="margin-right:10px">
            <span id="cart_count" class="badge badge-light"><?php echo $suma; ?></span>
          </li>
          </li>
        </ul>
      </div>
    </nav>
    <script>
      create_dreapta();
    </script>