<head>
    <?php
    echo file_get_contents("head.html");
    ?>
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/navbar_admin.css">
</head>
<?php
    include('util/DB.php');
    if (isset($_SESSION['isLogged']))
      $isLogged = $_SESSION['isLogged'];
    else
      $isLogged = 0;
      ?>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse center"  id="navbarColor01">
        <ul class="navbar-nav item-center mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="home.php">
              <svg class="bi bi-house-fill" width="1.5em" height="1.5em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.5 12.995V16.5a.5.5 0 01-.5.5H4a.5.5 0 01-.5-.5v-7a.5.5 0 01.146-.354l6-6a.5.5 0 01.708 0l6 6a.5.5 0 01.146.354v7a.5.5 0 01-.5.5h-4a.5.5 0 01-.5-.5V13c0-.25-.25-.5-.5-.5H9c-.25 0-.5.25-.5.495z">
                </path>
                <path fill-rule="evenodd" d="M15 4.5V8l-2-2V4.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd">
                </path>
              </svg>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="produse_admin.php">Produse</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="clienti_admin.php">Clienti</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="comenzi_admin.php">Comenzi</a>
          </li>

        </ul>
      </div>
    </nav>