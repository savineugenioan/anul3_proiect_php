<?php
include("head.html");
?>
<link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<script>

</script>

<body>
    <?php
    include("navbar.php");
    ?>

    <form class="form-group " id="form" action="util/c_cont.php" method="post" onsubmit="return validate()">
            <p class="h4 mb-4 text-center">Creare Cont</p>
            <label for="textInput">Username</label>
            <input type="text" id="username" name="username" class="form-control mb-4" placeholder="Username">

            <label for="passwdInput">Parola</label>
            <input type="password" id="parola" class="form-control mb-4" placeholder="Parola">

            <label for="passwdInput">Confirmare Parola</label>
            <input type="password" id="parola_confirm" name="parola" class="form-control mb-4" placeholder="Parola">

            <label for="emailInput">Emaill</label>
            <input type="email" id="email" name="email" class="form-control mb-4" placeholder="Email">

            <label for="textInput">Adresa</label>
            <input type="text" id="adresa" name="adresa" class="form-control mb-4" placeholder="Adresa">

            <button class="btn btn-info btn-block my-4" >Creare Utilizator</button>
    </form>
    <script>
    function validate(){
        let username = document.getElementById('username').value;
        let parola = document.getElementById('parola').value;
        let parola_confirm = document.getElementById('parola_confirm').value;
        let email = document.getElementById('email').value;
        let adresa = document.getElementById('adresa').value;
        let alert1 = document.getElementById('alert1');
        
        if(username =="" || parola =="" || parola_confirm=="" || email =="" || username =="" || adresa ==""){
            if(alert1 == null){
                string = '<div class="alert alert-danger" id="alert1" role="alert">Completati toate datele!</div>';
                let div = document.createElement("div");div.innerHTML = string;
                console.log(div);
                let form = document.getElementById('form');
                form.insertBefore(div,form.childNodes[22]);
                return false;
            }
            alert1.innerText = "Completati toate datele!";
            return false;
        }
        if(parola != parola_confirm){
            if(alert1 == null){
                string = '<div class="alert alert-danger" id="alert1" role="alert">Parola nu coincide cu Confirmarea Parolei!</div>';
                let div = document.createElement("div");div.innerHTML = string;
                console.log(div);
                let form = document.getElementById('form');
                form.insertBefore(div,form.childNodes[22]);
                return false;
            }
            alert1.innerText = "Parola nu coincide cu Confirmarea Parolei!";
            return false;

        return false;
        }
        return true;
    }
    </script>
</body>