<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include("head.html");
  $def_source = "dragon.jpg";
  ?>

</head>

<body>
  <?php
  include("navbar.php");
  $sql = "SELECT * FROM PRODUSE";
  $query = mysqli_query($dbconnect, $sql);
  while ($row = $query->fetch_assoc()) {
    if (isset($_SESSION['cart'][$row['Id_Produs']])) {
      $v[$row['Id_Produs']]['count'] = $_SESSION['cart'][$row['Id_Produs']]['count'];
    } else {
      $v[$row['Id_Produs']]['count'] = 0;
    }
    $v[$row['Id_Produs']]['Id_Produs'] = $row['Id_Produs'];
    $v[$row['Id_Produs']]['Denumire'] = $row['Denumire'];
    $v[$row['Id_Produs']]['Pret'] = $row['Pret'];
    $v[$row['Id_Produs']]['Image'] = $row['Image'];
    //echo var_dump($v);
  }
  ?>

  <?php
  foreach ($v as $key=>$value) {
    //echo var_dump($value)." ";
    $image= $v[$key]['Image'];
    echo "
  <div class='card' >
    <img src='$image' class='card-img-top' onerror='this.src =".'"'.$def_source.'"'."' alt='...'>
    <div class='card-body'>
      <h5 class='card-title'>" . $v[$key]['Denumire'] . "</h5>
      <p class='card-text'>" . $v[$key]['Pret'] . "</p>
    </div>
    <div class='card-footer'>
      <button class='card-footer-item btn btn-primary' style='float:left;' onclick='scot_produs(this,".$v[$key]['Id_Produs'].");'>-</button>
      <span class='card-footer-item' style='top: 10%;' id='produs_count'>";
      if ($v[$key]['count'] == 0)
      echo "Add Item To Cart";
      else
      echo $v[$key]['count'];
      echo "
      </span>
      <button class='card-footer-item btn btn-primary' style='float:right;' onclick='adaug_produs(this,".$v[$key]['Id_Produs'].");'>+</button>
      </div>
  </div>";
  }
  ?>
  <script>
    var cart_count = document.getElementById('cart_count');
    function adaug_produs(el, id) {
      let x = parseInt(cart_count.innerText);
      let y = el.parentNode.childNodes[3];
      //console.log(parseInt(y.innerText));
      if(isNaN(parseInt(y.innerText))){
                y.innerText = 1;
      }
      else
         y.innerText = parseInt(y.innerText) +1;
      let suma = x;
      cart_count.innerText = suma + 1;
      post_prod('+',el.parentNode.parentNode.childNodes[3].childNodes[1].innerText,id);

    }
    function post_prod(op,el,id){
      console.log(op+' '+el+' '+' '+id);
      $.post('util/cart_modif.php', {
        op: op,
        produs: el,
        id:id
      });
    }

    function scot_produs(el,id) {
      let x = parseInt(cart_count.innerText); // cart
      let y = el.parentNode.childNodes[3];  // produs
      console.log(id);
      //console.log(parseInt(y.innerText));
      if (!isNaN(parseInt(y.innerText))) {
        let suma = x;
        cart_count.innerText = suma - 1;
        if(parseInt(y.innerText) > 1)
          y.innerText = parseInt(y.innerText) -1;
        else{
          y.innerText="Add Item To Cart";
      }
        post_prod('-',el.parentNode.parentNode.childNodes[3].childNodes[1].innerText,id);
      }
      else{
        y.innerText="Add Item To Cart";
      }

    }
  </script>

</html>