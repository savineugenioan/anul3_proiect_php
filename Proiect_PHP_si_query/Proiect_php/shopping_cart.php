<!DOCTYPE html>
<html lang="en">
<head>
<?php
  echo file_get_contents("head.html");
  include("navbar.php");
  $def_source = "dragon.jpg";
?>
<?php

$sql ="SELECT * FROM PRODUSE";
$query = mysqli_query($dbconnect,$sql)
or die(mysqli_error($dbconnect));

while ($row = mysqli_fetch_array($query)) {
$v[$row[0]]['Denumire'] = $row[1];
$v[$row[0]]['Pret'] = $row[2];
$v[$row[0]]['Image'] = $row[3];
//echo var_dump($v);
}

?>
<body>

<div style="display: block;height:50px">
<button style='float:left;margin-left:20px;margin-bottom:20px;'onclick="golire()" class="btn btn-primary">Goleste Lista</button>
<button style='float:right;margin-right:20px;margin-bottom:20px;' onclick="postare_comanda()" class="btn btn-primary">Plaseaza comanda!</button>
</br>
</div>

<?php
if(isset($_SESSION['cart']))
if(count($_SESSION['cart'])==0){
  unset($_SESSION['cart']);
}
if(isset($_SESSION['cart'])){
echo '
<table id="tabel_cump" class="table table-dark" style="text-align:center">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Produs</th>
      <th scope="col">Cantitate</th>
      <th scope="col">Pret Unitar</th>
      <th scope="col">Pret Total</th>
    </tr>
  </thead>
  <tbody>
';
foreach($_SESSION['cart'] as $key=>$value){
  $image = $v[$key]['Image'];
  echo "
    <tr>
      <th scope='row' ><img  class='t_img' src='$image' onerror='this.src =".'"'.$def_source.'"'."' alt='...'></th>
      <td >".$v[$key]['Denumire']."</td>
      <td>".$value['count']."</td>
      <td>".$v[$key]['Pret']." Lei</td>
      <td>".$v[$key]['Pret']*$value['count']." Lei</td>";
}
echo '
  </tbody>
</table>';
}
else{
  echo '
  <div style="display: block;" class="container">
  </br>
  <h6 class="lead">Cos de Cumparaturi!</h6>
  <hr class="my-4">
  <p id="mesaj">Cosul de cumparaturi este gol!</p>
</div>';
}
?>
</body>
<script>
  function golire(){
    $.post('util/golire_cart.php');
    window.location='shopping_cart.php';
  }
  function postare_comanda(){
    $.get('util/postare_comanda.php',{p:'p'},
    function(data,txt){
      //console.log(txt);
      var element = document.getElementById("tabel_cump");
      if(element){
          element.parentNode.removeChild(element);
      var div = document.createElement('div');
      if(data==1){
          $.post('util/golire_cart.php');
          var string =   '<div style="display: block;" class="container"></br>'+
            '<h6 class="lead">Comanda Plasata!</h6>'+
            '<hr class="my-4">'+
            '<p id="mesaj">Comanda dumneavoastra a fost plasata!</p>'+
          '</div>';
          div.innerHTML=string;
          //console.log('1');
          document.getElementById('cart_count').innerText = 0;;
          document.body.appendChild(div);
      }
      else{
          var string =   '<div style="display: block;" class="container"></br>'+
            '<h6 class="lead">Cos de Cumparaturi!</h6>'+
            '<hr class="my-4">'+
            '<p id="mesaj">Cosul de cumparaturi este gol!</p>'+
          '</div>';
          div.innerHTML=string;
          document.appendChild(div);
          //console.log('0');
      }
      }
    });
  }
</script>
</html>